import sys

from copy import deepcopy

import click

from tqdm import tqdm

from matplotlib.patches import Polygon


from data import (
    polynom_arr,
    obstacles_arr,
    entrance_arr,
    rect_height,
    rect_width,
)

from utils.primitives import (
    Polynom,
    Obstacle,
    Point2D,
    Vector2D,
    DIRECTIONAL_VECTORS,
    X_AXIS,
    Y_AXIS,
)
from utils.draw import plot_figures
from utils.find import (
    find_options
)


@click.group(context_settings={'help_option_names': ['-h', '--help']})
@click.pass_context
def cli(ctx):
    pass


@cli.command(name='draw', help='')
@click.option(
    '--output', '-o',
    default="/tmp/poly.png",
    type=click.Path(exists=False),  help="Output file")
@click.option(
    '--solutions', '-c',
    type=click.IntRange(1, 100),
    default=1,
    help="The number of the best 'rectangles' rendered in the solution")
@click.option(
    '--step', '-s',
    type=click.IntRange(1, 100),
    default=10,
    help="Moving step of rectangle")
@click.option(
    '--debug', '-d',
    is_flag=True,
    help="Debug mode")
@click.pass_obj
def draw(
        ctx,
        output,
        solutions,
        step,
        debug,
):
    polynom = Polynom(polynom_arr)
    polynom_points = list(polynom.get_anticlockwise())

    obstacles = [
        Obstacle(
            Point2D(
                x=arr[X_AXIS],
                y=arr[Y_AXIS]
            ),
        )
        for arr in obstacles_arr
    ]
    entrances = [
        Point2D(
            x=arr[X_AXIS],
            y=arr[Y_AXIS]
        )
        for arr in entrance_arr
    ]
    poly = Polygon(polynom_arr)

    figures = list()
    figures.append(poly)

    all_options = []

    for i in tqdm(range(0, len(polynom_points) - 1, 1)):

        start_point, end_point = polynom_points[i:i + 2]

        wall_vector = Vector2D(deepcopy(start_point), deepcopy(end_point))

        wall_norm = wall_vector.norm()

        if wall_norm < rect_width and wall_norm < rect_height:
            continue

        wall_direction = None
        for direction, sample_vector in DIRECTIONAL_VECTORS.items():
            if Vector2D.is_unidirectional(wall_vector, sample_vector):
                wall_direction = direction
                break

        if wall_direction is not None:
            all_options.extend(
                find_options(
                    direction=wall_direction,
                    wall_points=(
                        deepcopy(start_point),
                        deepcopy(end_point),
                    ),
                    rect_sizes=(
                                   rect_width,
                                   rect_height,
                    ),
                    obstacles=deepcopy(obstacles),
                    polynom_points=deepcopy(polynom_points),
                    entrances=deepcopy(entrances),
                    step=step,
                ),
            )
    data = sorted(
        all_options,
        key=lambda x: x.aptitude,
    )
    rectangles = [row.rectangle for row in data]
    figures.extend([
        rect.prepare_to_plot()
        for rect in rectangles[
                    :solutions if not debug else len(rectangles)
                    ]
    ])

    p = plot_figures(figures, obstacles_arr, entrance_arr)
    if not all_options:
        p.title("Solution not found")
    else:
        p.title("Solutions")

    try:
        p.savefig(fname=output)
        click.echo(output)
    except (FileNotFoundError, PermissionError, IsADirectoryError):
        click.echo("Cannot open file: '%s'" % output)
        sys.exit(1)


if __name__ == "__main__":
    cli()
