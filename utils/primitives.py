from enum import Enum

import numpy as np

from copy import deepcopy

from matplotlib.patches import Rectangle

X_AXIS = 0
Y_AXIS = 1


class Orientation(Enum):
    HORIZONTAL = 1
    VERTICAL = 2


class Direction(Enum):
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4


class Primitive2D:
    x_axis = 0
    y_axis = 1

    orthogonal_matrix = np.array([
                [1, 0],
                [0, -1]
            ])


class Point2D(Primitive2D):

    def __init__(self, x, y):
        self.arr = np.array([x, y])

    @property
    def x(self):
        return self.arr[self.x_axis]

    @x.setter
    def x(self, x):
        self.arr[self.x_axis] = x

    @property
    def y(self):
        return self.arr[self.y_axis]

    @y.setter
    def y(self, y):
        self.arr[self.y_axis] = y

    def __truediv__(self, other):
        return Point2D(self.x/other, self.y/other)

    def __add__(self, other):
        return Point2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point2D(self.x - other.x, self.y - other.y)

    def __str__(self):
        return 'Point2D(x=%s, y=%s)' % (self.x, self.y)

    def __repr__(self):
        return 'Point2D(x=%s, y=%s)' % (self.x, self.y)


class Vector2D(Primitive2D):

    def __init__(self, p1=None, p2=None, arr=None):
        if arr is None:
            v = p2 - p1
            self.arr = np.array([v.x, v.y])
        else:
            self.arr = arr

    def __str__(self):
        return 'Vector2D(x=%s, y=%s)' % (self.x, self.y)

    def __repr__(self):
        return 'Vector2D(x=%s, y=%s)' % (self.x, self.y)

    @property
    def x(self):
        return self.arr[self.x_axis]

    @x.setter
    def x(self, x):
        self.arr[self.x_axis] = x

    @property
    def y(self):
        return self.arr[self.y_axis]

    @y.setter
    def y(self, y):
        self.arr[self.y_axis] = y

    def norm(self):
        return np.linalg.norm(self.arr)

    def normalize(self):
        v = self.arr / np.linalg.norm(self.arr)
        return Vector2D(arr=v)

    def orthogonal(self):
        if self.x == 0 and self.y == 0:
            raise ValueError

        if self.y == 0:
            arr = np.array([self.y, self.x * -1])
            return Vector2D(arr=arr)

        elif self.x == 0:
            arr = np.array([self.y * -1, self.x])
            return Vector2D(arr=arr)

        arr = np.dot(
            self.orthogonal_matrix,
            self.arr,
        )
        return Vector2D(arr=arr)

    @staticmethod
    def is_unidirectional(v1, v2):
        lam = v1.norm()/v2.norm()

        if all(v2.arr * lam == v1.arr):
            return True
        elif all(v2.arr * lam * -1 == v2.arr):
            return False
        else:
            return False

    @staticmethod
    def is_collinear(v1, v2):
        arr = np.vstack([v1.arr, v2.arr])
        return np.linalg.det(arr) == 0

    @staticmethod
    def new_basis_matrix(v1, v2):
        return np.vstack([v1.arr, v2.arr]).transpose()


DIRECTIONAL_VECTORS = {
    Direction.UP:  Vector2D(arr=np.array([0, 1])),
    Direction.DOWN: Vector2D(arr=np.array([0, -1])),
    Direction.LEFT: Vector2D(arr=np.array([-1, 0])),
    Direction.RIGHT: Vector2D(arr=np.array([1, 0])),
}


class Polynom(Primitive2D):
    def __init__(self, arr: np.array):
        self.arr = arr

    def get_point_list(self):
        return [
            Point2D(p[self.x_axis], p[self.y_axis])
            for p in self.arr
        ]

    def is_clockwise(self, polynom_points) -> bool:
        """
        Signed area formula
        A = 1/2 * (x1*y2 - x2*y1 + x2*y3 - x3*y2 + ... + xn*y1 - x1*yn)
        """

        signed_area = 0
        for i in range(0, len(polynom_points) - 1, 1):
            start_point, end_point = polynom_points[i:i + 2]
            signed_area += start_point.x * end_point.y - start_point.y * end_point.x
        return signed_area < 0

    def get_anticlockwise(self):
        points = self.get_point_list()

        if self.is_clockwise(points):
            points.reverse()

        return points


class Obstacle(Primitive2D):
    def __init__(self, p: Point2D):
        self.p = p
        self.arr = p.arr

    def __str__(self):
        return "Obstacle(p=%s)" % self.p

    def __repr__(self):
        return "Obstacle(p=%s)" % self.p

    @property
    def x(self):
        return self.p.x

    @x.setter
    def x(self, x):
        self.p.x = x

    @property
    def y(self):
        return self.p.y

    @y.setter
    def y(self, y):
        self.p.y = y


class Entrance(Primitive2D):
    def __init__(self, start_point: Point2D, end_point: Point2D):
        self.start_point = start_point
        self.end_point = end_point


class Rect2D(Primitive2D):
    def __init__(
            self,
            vertical_length: float = 1.0,
            horizontal_length: float = 1.0,
            left_lower_point: Point2D = Point2D(0, 0),
    ):
        self.vertical_length = vertical_length
        self.horizontal_length = horizontal_length
        self.left_lower = left_lower_point
        self.left_upper = Point2D(
            x=left_lower_point.x,
            y=left_lower_point.y + vertical_length,
        )
        self.right_lower = Point2D(
            x=left_lower_point.x + horizontal_length,
            y=left_lower_point.y,
        )
        self.right_upper = Point2D(
            x=left_lower_point.x + horizontal_length,
            y=left_lower_point.y + vertical_length,
        )
        self.arr = np.empty((0, 2), float)
        self.arr = np.vstack(
            [
                self.left_lower.arr,
                self.left_upper.arr,
                self.right_lower.arr,
                self.right_upper.arr,
            ])

    def prepare_to_plot(self, facecolor='red'):
        return Rectangle(
            xy=(
                self.left_lower.x,
                self.left_lower.y,
            ),
            width=self.horizontal_length,
            height=self.vertical_length,
            facecolor=facecolor,
        )

    def step_right(self, step: float = 1.0):
        self.left_lower.x += step
        self.right_lower.x += step
        self.left_upper.x += step
        self.right_upper.x += step

    def step_left(self, step: float = 1.0):
        self.left_lower.x -= step
        self.right_lower.x -= step
        self.left_upper.x -= step
        self.right_upper.x -= step

    def step_up(self, step: float = 1.0):
        self.left_lower.y += step
        self.right_lower.y += step
        self.left_upper.y += step
        self.right_upper.y += step

    def step_down(self, step: float = 1.0):
        self.left_lower.y -= step
        self.right_lower.y -= step
        self.left_upper.y -= step
        self.right_upper.y -= step

    def check_polynom_points_intersects(self, polynom_points):
        for i in range(0, len(polynom_points), 1):
            if i == len(polynom_points) - 1:
                continue

            p1, p2 = polynom_points[i:i + 2]

            middle = (p1 + p2) / 2

            if self.find_wall_point_in(middle):
                return False

            if self.is_rect_segments_intersect(p1, p2):
                return False

        return True

    def check_polynom_points_inside_rect(self, polynom_points):
        for point in polynom_points:
            if self.find_wall_point_in(point):
                return False

        return True

    def check_obstacles_inside_rect(self, obstacles):
        for point in obstacles:
            if self.find_obstacle_in(point):
                return False

        return True

    def check_entrance(self, entrances):
        for i in range(0, len(entrances), 1):
            if i == len(entrances) - 1:
                continue

            p1, p2 = entrances[i:i + 2]

            if self.find_obstacle_in(p1) or self.find_obstacle_in(p2):
                return False

            middle = (p1 + p2) / 2

            if self.is_rect_segments_intersect(p1, p2):
                return False

            if self.find_obstacle_in(middle):
                return False

            if self.is_inside_segment(p1, p2):
                return False

        return True

    def is_can_located(self, obstacles, polynom_points, entrances):
        """Check if can we locate rect"""
        return all(
            [
                self.check_polynom_points_inside_rect(polynom_points),
                self.check_polynom_points_intersects(polynom_points),
                self.check_entrance(entrances),
                self.check_obstacles_inside_rect(obstacles),
            ]
        )

    def is_intersect(self, p1: Point2D, p2: Point2D,
                     p3: Point2D, p4: Point2D) -> bool:

        p3p4 = Vector2D(p3, p4)
        p3p1 = Vector2D(p3, p1)
        p3p2 = Vector2D(p3, p2)

        p1p2 = Vector2D(p1, p2)
        p1p3 = Vector2D(p1, p3)
        p1p4 = Vector2D(p1, p4)

        v1 = np.linalg.det(
            np.vstack([p3p4.arr, p3p1.arr])
        )
        v2 = np.linalg.det(
            np.vstack([p3p4.arr, p3p2.arr])
        )
        v3 = np.linalg.det(
            np.vstack([p1p2.arr, p1p3.arr])
        )
        v4 = np.linalg.det(
            np.vstack([p1p2.arr, p1p4.arr])
        )

        return v1 * v2 < 0 and v3 * v4 < 0

    def is_rect_segments_intersect(self, p1, p2):
        return any([
            self.is_intersect(p1, p2, p3, p4)
            for p3, p4 in [
                (self.left_lower, self.left_upper),
                (self.left_upper, self.right_upper),
                (self.right_upper, self.right_lower),
                (self.right_lower, self.left_lower),
            ]
        ])

    def find_wall_point_in(self, point: Point2D):
        """Is wall point inside rect
        |---------|
        |         |
        |  point  |
        |---------|
        """
        return (self.left_lower.x < point.x < self.right_upper.x and
                self.left_lower.y < point.y < self.right_upper.y)

    def find_obstacle_in(self, point: Point2D):
        """Is obstacle inside rect
        |--point--|
        |         |
        |         |
        |---------|

        |---------|
        |   point |
        |         |
        |---------|

        """
        return (self.left_lower.x <= point.x <= self.right_upper.x and
                self.left_lower.y <= point.y <= self.right_upper.y)

    def is_inside_segment(self, p1, p2):
        """
        (|, -) - rect side
        p1--|--------|--p2

        p2
        |
        |
        p1
                        p2
                        |
                        |
                        p1

        p1--|--------|--p2
        """

        if (self.left_upper.y == p1.y and
                p1.x < self.left_upper.x < p2.x):
            return True
        if (self.left_lower.y == p1.y and
                p1.x < self.left_lower.x < p2.x):
            return True
        if (self.left_lower.x == p1.x and
                p1.y < self.left_upper.y < p2.y):
            return True
        if (self.right_lower.x == p1.x and
                p1.y < self.right_upper.y < p2.y):
            return True

        return False

    def __str__(self):
        return "Rect2D(left_lower=%s, left_upper=%s, right_lower=%s, right_upper=%s)" % (
            self.left_lower,
            self.left_upper,
            self.right_lower,
            self.right_upper,
        )

    def __repr__(self):
        return "Rect2D(left_lower=%s, left_upper=%s, right_lower=%s, right_upper=%s)" % (
            self.left_lower,
            self.left_upper,
            self.right_lower,
            self.right_upper,
        )


def create_rect_horizontal_up_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_width,
            horizontal_length=rect_height,
            left_lower_point=Point2D(
                x=start_point.x - rect_height,
                y=start_point.y,
            ))


def create_rect_vertical_up_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_height,
            horizontal_length=rect_width,
            left_lower_point=Point2D(
                x=start_point.x - rect_width,
                y=start_point.y,
            ),
        )


def create_rect_horizontal_down_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_width,
            horizontal_length=rect_height,
            left_lower_point=Point2D(
                x=start_point.x,
                y=start_point.y - rect_width,
            ))


def create_rect_vertical_down_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_height,
            horizontal_length=rect_width,
            left_lower_point=Point2D(
                x=start_point.x,
                y=start_point.y - rect_height,
            ))


def create_rect_horizontal_left_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_height,
            horizontal_length=rect_width,
            left_lower_point=Point2D(
                x=start_point.x - rect_width,
                y=start_point.y - rect_height),
        )


def create_rect_vertical_left_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_width,
            horizontal_length=rect_height,
            left_lower_point=Point2D(
                x=start_point.x - rect_height,
                y=start_point.y - rect_width),
        )


def create_rect_horizontal_right_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_height,
            horizontal_length=rect_width,
            left_lower_point=deepcopy(start_point))


def create_rect_vertical_right_direction(
        start_point: Point2D,
        rect_width: float,
        rect_height: float,):
    return Rect2D(
            vertical_length=rect_width,
            horizontal_length=rect_height,
            left_lower_point=deepcopy(start_point))
