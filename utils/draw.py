import matplotlib.pyplot as plt

from matplotlib.collections import PatchCollection


def plot_figures(figures, obstacles_arr, entrance_arr):
    fig, ax = plt.subplots()

    p1 = PatchCollection(figures[:1], alpha=0.4)
    p2 = PatchCollection(figures[1:], alpha=0.4, facecolors='red')

    ax.add_collection(p1)
    ax.add_collection(p2)

    ax.scatter(obstacles_arr[:, 0], obstacles_arr[:, 1], color='red')
    ax.plot(entrance_arr[:, 0], entrance_arr[:, 1])

    plt.yscale('linear')
    plt.xscale('linear')
    plt.grid(True)

    return plt
