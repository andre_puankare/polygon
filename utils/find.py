from collections import namedtuple

from copy import deepcopy

from utils.primitives import (
    Vector2D,
    Orientation,
    Direction,
    create_rect_horizontal_up_direction,
    create_rect_horizontal_down_direction,
    create_rect_vertical_down_direction,
    create_rect_vertical_left_direction,
    create_rect_horizontal_left_direction,
    create_rect_horizontal_right_direction,
    create_rect_vertical_right_direction,
    create_rect_vertical_up_direction,
)


directional_rect_factory = {
    (Direction.UP, Orientation.HORIZONTAL): create_rect_horizontal_up_direction,
    (Direction.UP, Orientation.VERTICAL): create_rect_vertical_up_direction,
    (Direction.DOWN, Orientation.HORIZONTAL): create_rect_horizontal_down_direction,
    (Direction.DOWN, Orientation.VERTICAL): create_rect_vertical_down_direction,
    (Direction.LEFT, Orientation.HORIZONTAL): create_rect_horizontal_left_direction,
    (Direction.LEFT, Orientation.VERTICAL): create_rect_vertical_left_direction,
    (Direction.RIGHT, Orientation.HORIZONTAL): create_rect_horizontal_right_direction,
    (Direction.RIGHT, Orientation.VERTICAL): create_rect_vertical_right_direction,
}
directional_moving_rule = {
    Direction.UP:
        lambda rect, end_point:
            rect.right_upper.y <= end_point.y,
    Direction.DOWN:
        lambda rect, end_point:
            rect.left_lower.y >= end_point.y,
    Direction.LEFT:
        lambda rect, end_point:
            rect.left_upper.x >= end_point.x,
    Direction.RIGHT:
        lambda rect, end_point:
            rect.right_lower.x <= end_point.x,
}
directional_rect_moving_func = {
    Direction.UP: "step_up",
    Direction.DOWN: "step_down",
    Direction.LEFT: "step_left",
    Direction.RIGHT: "step_right",
}
directional_middle_of_rect_side_opposite_to_wall = {
    Direction.UP:
        lambda rect:
            (rect.left_lower + rect.left_upper) / 2,
    Direction.DOWN:
        lambda rect:
            (rect.right_lower + rect.right_upper) / 2,
    Direction.LEFT:
        lambda rect:
            (rect.left_lower + rect.right_lower) / 2,
    Direction.RIGHT:
        lambda rect:
            (rect.left_upper + rect.right_upper) / 2,
}


Options = namedtuple("Options", "aptitude rectangle")


def find_options(
        direction,
        wall_points,
        rect_sizes,
        obstacles,
        polynom_points,
        entrances,
        step=20,
):
    options = []
    start_point, end_point = wall_points
    rect_width, rect_height = rect_sizes

    middle_of_wall = (start_point + end_point) / 2
    for orientation in (Orientation.HORIZONTAL, Orientation.VERTICAL):
        rect_factory = directional_rect_factory.get(
            (direction, orientation),
            None,
        )

        if rect_factory is None:
            return []

        rect = rect_factory(
            start_point,
            rect_width,
            rect_height,
        )

        while directional_moving_rule[direction](rect, end_point):
            if rect.is_can_located(
                    polynom_points=polynom_points,
                    obstacles=obstacles,
                    entrances=entrances):
                middle_of_rect_side_opposite_to_wall = \
                    directional_middle_of_rect_side_opposite_to_wall[direction](rect)

                options.append(
                    Options(
                        aptitude=Vector2D(
                            middle_of_wall,
                            middle_of_rect_side_opposite_to_wall,
                        ).norm(),
                        rectangle=deepcopy(rect),
                    ),
                )
            getattr(rect, directional_rect_moving_func[direction])(step)

    return options
